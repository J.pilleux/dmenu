#ifndef TOKYONIGHT_H
#define TOKYONIGHT_H

static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c0caf5", "#1a1b26" },
	[SchemeSel] = { "#1a1b26", "#bb9af7" },
	[SchemeSelHighlight] = { "#1a1b26", "#bb9af7" },
	[SchemeNormHighlight] = { "#ffc978", "#1a1b26" },
	[SchemeOut] = { "#c0caf5", "#7dcfff" },
};

#endif
